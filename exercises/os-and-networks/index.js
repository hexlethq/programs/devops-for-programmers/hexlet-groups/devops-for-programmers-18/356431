const fastify = require('/usr/lib/node_modules/fastify')({
  logger: true,
})

fastify.get('/', async (request, reply) => {
  reply
  .type('application/json')
  .code(200);
  return {hello: 'world'};
});

fastify.listen(8080, '0.0.0.0', (err, address) => {
  if (err) throw err
});
