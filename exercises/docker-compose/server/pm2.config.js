module.exports = {
    apps : [
        {
            name: 'server',
            script: '/build/index.js',
            watch: false,
            restart_delay: 3000,
            pmx: false,
            kill_timeout: 1600,
            listen_timeout: 3000,
        },
    ]
};
